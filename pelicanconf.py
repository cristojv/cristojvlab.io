#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
AUTHOR = 'Cristo JV'
SITENAME = 'Cristo JV'
SITEURL = ''
GOOGLE_ANALYTICS = "UA-167947921-1"
DISQUS_SITENAME = 'cristojv'
USE_OPEN_GRAPH = True

#SETTINGS
USE_FOLDER_AS_CATEGORY = True
DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = True
DELETE_OUTPUT_DIRECTORY = True
OUTPUT_PATH = 'public/'
PATH = 'content'
PAGE_PATHS = ['pages']
ARTICLE_PATHS = ['']
STATIC_PATHS = ['static/assets/img/','static/assets/img/publications/']
RELATIVE_URLS = False
TIMEZONE = 'Atlantic/Canary'
DEFAULT_DATE_FORMAT = '%a %d %B %Y'
DEFAULT_LANG = 'en'
DIRECT_TEMPLATES = ['index', 'authors', 'categories', 'tags', 'archives']
DEFAULT_PAGINATION = 10
THEME = 'cristojv-theme/'
THEME_STATIC_PATHS = ['static']
FORMATTED_FIELDS = ['summary']
LINKEDING_LINK = 'https://www.linkedin.com/in/cristojv/'
STACKOVERFLOW_LINK = 'https://stackoverflow.com/users/5174565/cristojv'
GITLAB_LINK = 'https://gitlab.com/cristojv'
GITHUB_LINK = 'https://github.com/cristojv'
RESEARCHGATE_LINK = 'https://www.researchgate.net/profile/Cristo_Jurado_Verdu'