Title: VLC for Industry 4.0
Date: 2019-06-30 19:00
Author: Cristo JV
Cover: /static/assets/img/projects/vlc4industry/robot.jpg
CoverSize: Large
opengraph_image: /static/assets/img/projects/vlc4industry/robot.jpg
Summary: In this project our team at [IDeTIC](https://www.idetic.ulpgc.es/idetic/index.php/es/)'s Division of Photonic Technology and Communications develops the first spanish prototype of an industrial robotic control system using *LiFi* technology. This system uses two different communication links: a conventional Lifi downlink to deliver instructions and commands to the robot, and a novel Optical Camera Communication uplink that replaces traditional photoreceptors for camera sensors. This project aims to demonstrate the viability of *Lifi* within an industrial scenario, where industrial lightning and surveillance systems could serve as functional communication systems.

![Cover](/static/assets/img/projects/vlc4industry/robot-horizontal.jpg){:style="width:90% !important;"}

In this project our team at IDeTIC's Division of Photonic Technology and Communications develops the first spanish prototype of an industrial robotic control system using *LiFi* technology. This system uses two different communication links: a conventional Lifi downlink to deliver instructions and commands, and a novel Optical Camera Communication uplink that replaces traditional photoreceptors for camera sensors. If you want to get a more profound sense of how Optical Camcera Communication systems works check out [this]({filename}../articles/explainlikeimfive.md) article.

This project aims to demonstrate the viability of Lifi within an industrial scenario, where industrial lightning and survaillance systems could serve as practical communication systems.

The following video shows a clone demonstrator deployed at [IDeTIC](https://www.idetic.ulpgc.es/idetic/index.php/es/) labs. The video starts showing the flood light that sends instructions to the robot using a custom design Modulation based on Differential Pulse Position Modulation. It continues showing a vertical OCC Lamp that sends the robot's response to a rapsberry picamera.

<iframe width="560" height="475" src="https://www.youtube.com/embed/QAW25IGN9kI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>