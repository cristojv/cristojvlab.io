title: Application of Optical Camera Communication to Microalgae Production Plants
date: 2020-07-20
tags: Optical Camera Commnications, Visible Light Communications, Conference
cover: /static/assets/img/publications/csndsp2020_cover.png
opengraph_image: /static/assets/img/publications/csndsp2020_cover.png
category: publications
type: Conference
name: 2020 12th International Symposium on Communication Systems, Networks and Digital Signal Processing (CSNDSP), Porto
publisher: IEEE
authors: Cristo Jurado-Verdu, Victor Guerra, Vicente Matus, Jose Rabadan, Rafael Perez-Jimenez, Juan Luis Gomez-Pinchetti, Carlos Almeida
doi:10.1109/CSNDSP49049.2020.9249534
publicationlink:https://doi.org/10.1109/CSNDSP49049.2020.9249534
abstract: Optical Camera Communication (OCC) Systems have a potential application for monitoring flat-panel photobioreactors in microalgae production plants. On the transmitter side, the design of pulsed optical signals in terms of light intensity, spectral composition, and temporal variation is focused on optimizing the photosynthetic efficiency of the culture. At the reception, cameras are utilized for simultaneous online monitoring and in-situ sensing of the bioreactors culture. In this work, the analysis of the node arrangement in the plant has been carried out considering node visibility, data rate, and effective space utilization. Finally, a real experiment was performed to validate the application's feasibility.