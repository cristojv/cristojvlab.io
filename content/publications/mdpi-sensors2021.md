title:Optical Camera Communication as an Enabling Technology for Microalgae Cultivation
date: 2021-01-01
tags: Optical Camera Communications, Visible Light Communications, Journal
cover: /static/assets/img/publications/mdpi-sensors2021_cover.png
opengraph_image: /static/assets/img/publications/mdpi-sensors2021_cover.png
category: publications
type: Journal
name: MDPI Sensors (Special Issue Visible Light Communication, Networking, and Sensing), V.21, Issue 15 (pp. 1621)
publisher: Multidisciplinary Digital Publishing Institute
authors: Cristo Jurado-Verdu, Victor Guerra, Vicente Matus, Carlos Almeida, Jose Rabadan
doi:10.3390/s21051621
publicationlink:https://doi.org/10.3390/s21051621
abstract: Optical Camera Communication (OCC) systems have a potential application in microalgae production plants. In this work, a proof-of-concept prototype consisting of an artificial lighting photobioreactor is proposed. This reactor optimises the culture’s photosynthetic efficiency while transmitting on-off keying signals to a rolling-shutter camera. Upon reception, both signal decoding and biomass concentration sensing are performed simultaneously using image processing techniques. Moreover, the communication channel’s theoretical modelling, the data rate system’s performance, and the plant distribution requirements and restrictions for a production-scale facility are detailed. A case study is conducted to classify three different node arrangements in a real facility, considering node visibility, channel capacity, and space exploitation. Finally, several experiments comprising radiance evaluation and Signal-to-Noise Ratio (SNR) computation are performed at different angles of view in both indoor and outdoor environments. It is observed that the Lambertian-like emission patterns are affected by increasing concentrations, reducing the effective emission angles. Furthermore, significant differences in the SNR, up to 20 dB, perceived along the illuminated surface (centre versus border), gradually reduce as light is affected by greater dispersion. The experimental analysis in terms of scattering and selective wavelength attenuation for green (Arthrospira platensis) and brown (Rhodosorus marinus) microalgae species determines that the selected strain must be considered in the development of this system. 