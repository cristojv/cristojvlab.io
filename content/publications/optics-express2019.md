title: Correlation-based receiver for optical camera communications
date: 2019-07-08
author: Cristo JV
tags: Optical Camera Communications, Visible Light Communications, Journal
cover: /static/assets/img/publications/optic-express2019_cover.jpg
opengraph_image: /static/assets/img/publications/optic-express2019_cover.jpg
category: publications
type: Journal
name: Optics Express, V. 27, Issue 14 (pp. 19150-19155)
publisher: Optical Society of America
authors: Cristo Jurado-Verdu, Vicente Matus, Jose Rabadan, Victor Guerra, Rafael Perez-Jimenez
doi:10.1364/OE.27.019150
publicationlink:https://doi.org/10.1364/OE.27.019150
abstract: In color-multiplexed optical camera communications (OCC) systems, data acquisition is restricted by the image processing algorithm capability for fast source recognition, region-of-interest (ROI) detection and tracking, packet synchronization within ROI, estimation of inter-channel interference and threshold computation. In this work, a novel modulation scheme for a practical RGB-LED-based OCC system is presented. The four above-described tasks are held simultaneously. Using confined spatial correlation of well-defined reference signals within the frame’s color channels is possible to obtain a fully operating link with low computational complexity algorithms. Prior channel adaptation also grants a substantial increase in the attainable data rate, making the system more robust to interferences.
<!-- Link: https://www.osapublishing.org/DirectPDFAccess/893264A8-8B96-4FA1-ACD88E853F4F3F16_414736/oe-27-14-19150.pdf?da=1&id=414736&seq=0&mobile=no -->