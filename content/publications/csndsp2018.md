Title: RGB Synchronous VLC modulation scheme for OCC
Date: 2018-07-18
Author: Cristo JV
Tags: Optical Camera Communications, Visible Light Communications, Conference papers
Cover: /static/assets/img/publications/csndsp2019_cover.png
opengraph_image: /static/assets/img/articles/occ_explainlikeimfive/cover.png
authors: Cristo Jurado-Verdu, Victor Guerra, Jose Rabadan, Rafael Perez-Jimenez, Patricia Chavez-Burbano
type: Conference
name: 2018 11th International Symposium on Communication Systems, Networks & Digital Signal Processing (CSNDSP)
publisher: IEEE
doi:10.1109/CSNDSP.2018.8471829
publicationlink:https://doi.org/10.1109/CSNDSP.2018.8471829
abstract: Nowadays Optical Camera Communications (OCC) have acquired an important relevance within the Visible Light Communications' (VLC) field. However, this technique faces challenges related to computational load and packet synchronization. In this work, a synchronous modulation scheme for OCC is presented. It is based on the wavelength-division multiplexing of RGB light sources for the generation of two different signals. The first one carries the data, using two of the independent channels, while the other provides the transmission clock and frame synchronization. In this way, the demodulation is significantly improved, since the clock recovery process and the frame detection are highly simplified.