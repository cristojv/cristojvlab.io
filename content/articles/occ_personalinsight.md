Title: Is Optical Camera Communication (OCC) a disruptive communication technology?
Date: 2021-10-05 13:57
Author: Cristo Jurado-Verdu
Tags: Visible Light Communications, Optical Camera Communications, Li-Fi
Cover: /static/assets/img/articles/occ_personalinsight/cover.JPG
Coversize: Large
opengraph_image: /static/assets/img/articles/occ_personalinsight/cover.JPG
Summary: Are lamp to camera communications suitable for future society communications?. Which are the **advantages** and **disadvantages** of the LiFi techonology and, especially the **Optical Camera Communication** (OCC) technology?. How VLC and OCC can offer new ways for users to interact with networks?.

<!-- Status: draft -->

---------------------------------------

##### **Notes from the author**

The purpose of this post is to share a personal opinion and start an open discussion.
This analysis came from the experience I gained from developing OCC applications and collaborating with other researchers since I started investigating this field.
However, I am still an inexperienced researcher, and I greatly value any contribution.
Thus, feel free to make any contribution, revision, or feedback in the comments section. They are more than welcome, and they will be helpful to deliver quality content!.

---------------------------------------

I believe that all the 
engineers and researchers that work in the field of OCC have once wondered in their careers if **OCC is a disruptive market technology...**

By the time this question came up in my mind, I was participating in the CSNDSP18 conference (in Budapest). This was my first talk at this kind of event. I had the opportunity to present the OCC prototype we developed at the IDeTIC labs.

I was extremely nervous... yet everything went well at the end... until the turn for questions started.

<!-- ... I was able to barely manage my nerves and wait patiently for the turn of questions. -->

And... this question arises from a well-renowned doctor in the field of optical communications, Dr. Zabih Ghassemlooy.

![Conference](/static/assets/img/articles/occ_personalinsight/example.JPG){:style="width:75% !important"}
*OCC prototype.*

> ### *What do you think is a potential application of this OCC technology?*

I panicked...  it was the kind of simple yet complex question I was not prepared to answer... I also forgot that I will present, later on, that day, a potential application of this technology regarding **precisely indoor localization**. Using OCC to estimate with high accuracy the transmitter (a lamp source) position in a room.

By that time, I focused my efforts on developing this technology setting aside its **usefulness** or **viability**.
I wasn't indeed centering on real potential applications or needs. So my career starts to deviate from theory to practice, beginning our journey to

> ### Find that killer and successful OCC application!

To start this journey, it is still necessary to sit back and identify the **key differences** between **OCC** and conventional **radio** communications, like wifi, 4G, 5G, among others...

Let's highlight and summarize some statements I have collected in seminars, conferences, popular videos, articles, and occasional conversations to open up the discussion.

---------------------------------------

**Note from the author: ** Now, I will briefly detail the advantages and disadvantages of VLC and OCC. However, feel free to jump directly to my conclusions at the end of this article.


#### **1) Capacity**:

---------------------------------------

> ##### "The visible spectrum is vast, hundreds of times greater than the radio spectrum. Therefore, there is more space to distribute our communications".

---------------------------------------
I agree with this statement. The visible light spectrum range is **vast**, greater than the radio spectrum. This capacity can either increase the data rate between VLC devices or provide coverage to many devices.

![Spectrum](/static/assets/img/articles/occ_personalinsight/spectrum.png){:style="width:75% !important"}
*Electromagnetic spectrum. Highlighted the Visible Ligth Spectrum. Note: it is not in scale, check dimensions.*

However, we must also consider how light is generated using LED sources to efficiently use this capacity.

LED emits light in specific ranges of the spectrum through an electron-hole recombination process that releases energy in the form of photons. I'm not going to go deeper into this concept. However, if you want to have a detailed explanation of this process, check this Wikipedia [link](https://en.wikipedia.org/wiki/Light-emitting_diode) or check this [book](https://books.google.ro/books?hl=es&lr=&id=ZgsqDwAAQBAJ&oi=fnd&pg=PP1&dq=Visible+Light+Communications+Theory+and+Applications+Zabih+Ghassemlooy+Citation&ots=hvhKJsLgy0&sig=Zlyx-SMy5AQyO0_3fXvpljV3zlo&redir_esc=y#v=onepage&q=Visible%20Light%20Communications%20Theory%20and%20Applications%20Zabih%20Ghassemlooy%20Citation&f=false) chapter (pages 23, 24).

This recombination process confers to the LED its specific spectrum emission, in other words, the set of electromagnetic waves it emits (which is strongly determined by the fabrication process). Different spectrum examples are shown in the following figure (White, Red, Green, and Blue colored LEDs). As can be seen, Blue LEDs emit in a very narrow portion of the spectrum (that we perceive as blueish), while white LEDs emit in the whole range.

![Spectrum](/static/assets/img/articles/occ_personalinsight/ledspectrum.png){:style="width:75% !important"}
*Electromagnetic spectrum of LEDs. White LED vs Red, Green and Blue LEDs.*

If we use just plain white LEDs, we will not benefit from the total capacity VLC provides. Using the entire spectrum for one exclusive link is highly inefficient. Therefore the spectrum range must be divided into narrow slots or portions for different purposes. This is done within the radio spectrum, where TV links use a specific division fully independent from 4G links. The radio spectrum distribution is shown in the following figure.

![Spectrum](/static/assets/img/articles/occ_personalinsight/us_frequency_allocations.jpg){:style="width:75% !important"}
*US radio frequency allocations map (2016). [Link](https://en.wikipedia.org/wiki/Frequency_allocation) to the wikipedia.*

In short, if we want to fully exploit the available spectrum, we need to use LEDs with narrow emission spectrums or use appropriate optical filters.

**In conclusion, despite the considerable extension of the visible spectrum, it does not directly imply that we can exploit it efficiently, thus affecting capacity and/or data speed.**

Also, LED's intensity variations are restricted in speed. LEDs do not switch on and off as fast as we want them to do it which constraints, even more, the data throughput.

Finally, as it was introduced in my previous article, "[Explain Like I'm Five: OCC]({filename}../articles/occ_explainlikeimfive.md)," regarding OCC, the data rate is also constrained by the camera sampling rate.

In conclusion, OCC systems are constrained in bandwidth. However, many actual VLC experiments perform far better than conventional radio systems, with data rates exceeding the hundreds of Gigabytes per second. Therefore capacity is a differentiating factor. Still, as engineers, we must solve these issues to get the most of OCC/VLC systems. **

#### **2) Efficiency**:
---------------------------------------

> ##### "There is high energy involved in refrigerating radio cellular and base stations. So in terms of data efficiency, VLC could differentiate".

---------------------------------------
I remember when I hear this for the first time. It was in the Harald Hass TED talk that is shown in the following video.
<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/lang/en/harald_haas_wireless_data_from_every_light_bulb" width="560" height="315" style="position:absolute;left:0;top:0;width:100%;height:100%; margin-bottom:100px" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>
*Harald Hass TED talk.*

This is a remarkably controversial statement that I personally do not share. Yes, high power base stations indeed consume a lot of energy to refrigerate radio systems and antennas. So technically, the energy involved in the radio operation is not used just for communications. The system is relatively inefficient.

However, LEDs, specifically LEDs used for illumination, also need refrigeration. This refrigeration could be passive when the LED is operating in standard conditions (illuminating). But when it is switching at an exceptionally high speed, powerful cooling (active) is needed. If there is no refrigeration, as soon as the temperature increases, the LED will behave differently than expected, changing its switching speed, its intensity level, or even its spectrum.

Besides refrigeration, LEDs also have an inherent quantum efficiency related to the optical power emitted with the injected electrical power. Despite that Organic LEDs provides relatively high quantum efficiency, it is still essential to increase further the LED efficiency to achieve low power transmissions.

Finally, it must be taken into account that LEDs are used for illumination, so it is not fair to compare both technologies. Leaving aside this illumination purpose... could we compare a small Organic LED with a low-power radio system in terms of efficiency? We should do the math, but **I will not say that efficiency is a differentiating factor of VLC and OCC technology.**

#### **3) Availability**:
---------------------------------------

> ##### "Light could be deployed in places that are sensitive to radio electromagnetic interferences, such as airplanes or hospitals."

---------------------------------------
I completely agree with this statement. Light can not interact with conventional radio antennas, at least not how it is intended for a radio wave. By saying this, I mean that light could interact with the **exposed** silicon of some electronic components. As a matter of fact, in these links ([link1](https://www.wired.com/story/lasers-hack-amazon-echo-google-home/), 
[link2](https://lightcommands.com/)), researchers have created a laser transmitter that sends commands to a google assistant's microphone!. The microphone itself is fabricated using silicon. The light that reaches this exposed silicon surface (as it were a photodiode) generates an output current that emulates a voice. Check the following video that shows the general operation of the system.

<div>
<iframe width="560" height="315" src="https://www.youtube.com/embed/ihRAwc24nXw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<em> Hacking google assistant with a laser pointer </em>
</div>

In general terms, light can be used safely in places where the radiowaves could interfere with electronic equipment, such as airplanes or hospitals. **And that is a potential differentiating factor!**

#### **4) Security**:
---------------------------------------

> ##### "VLC systems are more secure than radio systems".

---------------------------------------
There is always confusion when labeling VLC as a secure communication technology. What should be understood when someones affirm that VLC is more secure than radio?. Well, it's a little bit tricky. If someone wants to access your data, they could access the communication channel either using a photodiode (VLC) or an antenna (radio). Both communication links can be attacked.
Similarly, It is possible to listen to an ethernet cable (Just to let you know it is not that easy :P). Therefore, only if the data is fully encrypted, the hacker or your neighbor would not be able to read your critical messages. And that is true for both technologies.

The fundamental difference lies in the confinement easiness of the light. Light is naturally opaque to most structural materials like walls and the furniture present in houses. Therefore it is easier to confine light in an area, denying access to external spies. 
You can also isolate your radio communications by covering your house with a metal structure, but that sounds expensive and for sure will raise suspicions.

However, this property of the light can also be viewed as a disadvantage. For example, if you cover your receiver (your smartphone's camera), you can not access the network.

So there are two sides of a coin regarding this aspect. It can be seen either as an advantage or a disadvantage. However, what is true is that the user has, in the end, greater control over the network.

#### **5) Reusability**:
---------------------------------------

> ##### "I already have a light bulb at home, let's use it!".

---------------------------------------

I love this term. As engineers, we love using things for applications that weren't intended. We, as users, see a lamp, but as engineers, we see a communication device. The same for a conventional camera... 
This concept of reusing devices for different purposes is tempting by passionate engineers and the market. If you can reuse, you can grow faster in markets, and your technology will be market-ready sooner. Fast and simple. You already got the devices; just tweak some parameters, make modifications, and let's start the communication.

#### **6) Sensitivity**:
---------------------------------------

> ##### "If I can see the light, I can sense the network."

---------------------------------------

The last statement is a personal point of view. It's is very simple, yet, as I will discuss later on in this article, it provides room for new communication paradigms.

Let's arrive at the fundamental key difference of this system, the human perception. Light is visible (I'm talking about the visible spectrum; there are high and low wavelengths that humans cannot see).

First, as light can be seen, there is a lot of issues to solve. VLC or OCC links must be safe for humans. It's desirable to remove the flickering that produces hassle, migraines, or other types of illnesses.

In addition, **do I have to turn on the lamps to connect to the internet?** Technically, yes. In terms of human awareness, this is an issue. Personally, I don't want a constant LED light pointing to my desktop in the morning. To overcome this problem, there are current solutions that use non-noticeable light (infrared). This similar technology uses your TV remote.

So we must follow what the standard of this technology states: the communications must be unnoticeable by humans, as wifi does already, with the difference that light can be seen. And this introduces significant technology constraints.

<hr>
Let's do a break :D.
<hr>

Let's answer now: **what is a potential application of this technology?**

We at our laboratories are still exploring novel ideas. But my personal answer to this tricky question that pushes me to continue passionately working in this field can be briefly summarized in the following statement.

> ### VLC and OCC technology is the way to achieve future semantic communications.

Maybe I have chosen the wrong set of words, but let me explain what I mean for *semantic* communications.

For semantic communications, I'm referring to the human meaning or perception of communications. And I'm not referring to the meaning the user gives to the information itself, but the human awareness or understanding of what establishing a network link is.

The sensitivity factor in VLC and OCC links (visible light) allows the user to extract meaning from the network itself. It can sense the network. The user knows where the network is deployed because he can see the falling light in his room, over the walls, on the floor. Also, he can understand the maximum distance the link can reach; more limited coverage would be in the darker areas of the room.

Therefore, the user senses the network: where it is deployed, whether it is working or not, whether there is an opportunity for future connections, or if the network is saturated. In addition, the network can react to specific events and send particular machine-to-human messages that the human can sense and interpret, such as alerts, notifications, updates, etc. For example, if the light turns red, it can mean that the network is alerting the user of low speed or that the LED cell is congested. Another example,
if the light can move through the scenario indicating the following location for a guided tour in museums. So the place of the light guides you, and, at the same time, the light transmits the required information to your phone for the next display.

This creates a new paradigm for communications, new ways that an active user interacts with the network. It is opposed to conventional radio communications, invisible to the user, only knowing its state through a machine interface.

The network itself talks directly to the user. Consequently, the user gives some meaning to the light received by the lamp.

And from my personal perspective, this offers an incredible potential to change conventional user interaction with networks. Approaching humans with machines. 

I hope you have found this article interesting and valuable.

And you...

> ### *What do you think is a potential application of this OCC technology?*