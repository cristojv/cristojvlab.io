#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

# If your site is available via HTTPS, make sure SITEURL begins with https://
SITEURL = 'https://cristojv.com'
# RELATIVE_URLS = True
# STATIC_PATHS = ['static/assets/img/']

# FEED_ALL_ATOM = 'feeds/all.atom.xml'
# CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'

# DELETE_OUTPUT_DIRECTORY = True

# # Following items are often useful when publishing

# #DISQUS_SITENAME = ""
# #GOOGLE_ANALYTICS = ""

# #Gitlab integration
# OUTPUT_PATH = 'public/'

# #Custom theme
# THEME = 'cristojv-theme/'

# #Publish variables
# LINKEDING_LINK = 'https://www.linkedin.com/in/cristojv/'
# STACKOVERFLOW_LINK = 'https://stackoverflow.com/users/5174565/cristojv'
# GITLAB_LINK = 'https://gitlab.com/cristojv'
# GITHUB_LINK = 'https://github.com/cristojv'

# ABOUT_ME = "I'm a PHD student in Optical Communications at the University of Las Palmas, in Canary Islands. Currently, I'm developing novel strategies for data transmission between conventional light sources and smartphone's cameras (A recent branch derived from conventional LiFi Technology). In my free time I love doing agressive roller-blading, or going out to have a beer with friends, always leaving a space for reading a good book."